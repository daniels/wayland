option('build_docs',
       type: 'combo',
       choices: [ 'auto', 'yes', 'no' ],
       value: 'auto',
       description: 'Build documentation (requires Doxygen, xmlto, Graphviz)')
option('icon-dir',
       type: 'string',
       description: 'Path to search for cursor icons (defaults to datadir)')
